import { Injectable } from '@angular/core';
import { Firestore ,} from '@angular/fire/firestore';
import { addDoc, collection } from 'firebase/firestore';
import Place from '../interfaces/place.interface';
@Injectable({
  providedIn: 'root'
})
export class PlacesService {

  constructor(private firestore:Firestore) { }

  addPlace(place:Place){
    const placeRef = collection(this.firestore,'places')
    return addDoc(placeRef,place)
  }
}
